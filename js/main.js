function Limpiar() {
    document.getElementById('txtValor').value = "";
    document.getElementById('txtEnganche').value = "";
    document.getElementById('txtFinanciar').value = "";
    document.getElementById('txtPago').value = "";
    document.getElementById('cmbPlanes').value = 1;
}

function Calcular() {
    var valorAuto = document.getElementById('txtValor').value;
    var opcion = document.getElementById('cmbPlanes').value;
    var meses, intereses;
    var enganche = (valor* 0.3).toFixed(2);
    switch (opcion) {
        case '1':
            meses=12;
            intereses=0.125;
            break;
        case '2':
            meses=18;
            intereses=0.172;
            break;
        case '3':
            meses=24;
            intereses=0.21;
            break;
        case '4':
            meses=36;
            intereses=0.25;
            break;
        case '5':
            meses=48;
            intereses=0.45;
            break;
        default:
            alert("La opcion no es valida.");
            break;
    }
    document.getElementById('txtFinanciar').value = (valor - enganche + (valor * intereses)).toFixed(2);
    document.getElementById('txtEnganche').value = enganche;
    document.getElementById('txtPago').value = ((valor - enganche + (valor * intereses)) / meses).toFixed(2);
}